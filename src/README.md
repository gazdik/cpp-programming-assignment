# Getting Started

1. Install `conan` for `python3` (ideally using `pip`):

```shell
# for current user
pip3 install conan --user
# or system-wide
sudo pip3 install conan
```

2. Update `conanfile.txt` if needed.

3. Create a conan profile:

```shell
conan profile new default --detect
conan profile update settings.compiler.libcxx=libstdc++11 default
```

4. Update `conanfile.txt`, e.g. add new dependencies.

5. Install the required dependencies


```shell
mkdir build && cd build
conan install .. --build=missing
```
6. Update `CMakeLists.txt`, e.g. add new source files.

7. Build the app

```shell
(win)
cmake .. -G "Visual Studio 16"
cmake --build . --config Release

(linux, mac)
cmake .. -G "Unix Makefiles" -DCMAKE_BUILD_TYPE=Release
cmake --build .
```

If any of this does not work, please see [this tutorial](https://docs.conan.io/en/latest/getting_started.html)
