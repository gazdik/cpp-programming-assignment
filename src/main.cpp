#include <iostream>
#include <boost/program_options.hpp>

void parse_args(int argc, char** argv, std::string& retInputFile)
{
  namespace opt = boost::program_options;
  opt::options_description desc{"Options"};
  desc.add_options()
      ("help,h", "Help screen")
      ("input,i", opt::value<std::string>(&retInputFile), "Input file");
  opt::variables_map args;
  opt::store(parse_command_line(argc, argv, desc), args);
  notify(args);
  if(args.count("help"))
  {
    std::cout << desc << '\n';
    std::exit(0);
  }

  if(!args.count("input"))
    throw std::runtime_error("The input file missing. See help (option -h)");
}

int main(int argc, char** argv)
try
{
  std::string filePath;
  parse_args(argc, argv, filePath);
}
catch (const std::exception& e)
{
  std::cerr << "Exception: " << e.what() << std::endl;
}
catch(...)
{
  std::cerr << "Unknown exception" << std::endl;
}
